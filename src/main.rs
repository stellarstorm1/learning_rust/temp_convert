use std::io;

fn main() {
    let mut input_temp = String::new();
    println!("What temp do you want to convert?");
    io::stdin().read_line(&mut input_temp).expect("Failed to read line.");

    let mut input_scale = String::new();
    println!("What scale is the current temperature? Type either F or C");
    io::stdin().read_line(&mut input_scale).expect("Failed to read line.");

    let input_temp = input_temp.trim().parse::<f64>().unwrap();
    let input_scale = input_scale.trim();

    // Surely there's a better way to do this than calling println! twice...
    if input_scale == "F" {
        let out_scale = "C";
        let converted_temp: f64 = (input_temp - 32.0) * 5.0/9.0;
        println!("{} degrees {} is {} degrees {}", input_temp, input_scale,
                converted_temp, out_scale);
    } else if input_scale == "C" {
        let out_scale = "F";
        let converted_temp: f64 = (input_temp * 9.0/5.0) + 32.0;
        println!("{} degrees {} is {} degrees {}", input_temp, input_scale,
                converted_temp, out_scale);
    } else {
        println!("Unsupported scale!")
    }
}
